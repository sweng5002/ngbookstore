import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
  loginInvalid = false;
  login(formValues) {
    this.authService.loginUser(formValues.userName,
      formValues.password).subscribe(resp => {
        if (!resp) {
          this.loginInvalid = false;
        } else {
          this.router.navigate(['/home']);
        }
    });
  }

  constructor(public router: Router, private authService: AuthService) {
  }

  ngOnInit() {
  }

  onLoggedin() {
    localStorage.setItem('isLoggedin', 'true');
  }
}

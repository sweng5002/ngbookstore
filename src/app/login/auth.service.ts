import { Injectable } from '@angular/core';
import { IUser } from './user.model';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AuthService {
  currentUser: IUser;
  constructor(private http: Http) {
  }
  loginUser(userName: string, password: string) {
    const headers = new Headers({ 'Content-Type': 'application/json', 'service': 'userManagement', 'operation': 'login'});
    const options = new RequestOptions({headers: headers});
    const loginInfo = { username: userName, password: password };

    return this.http.post('http://18.221.46.7:3010/apigateway', JSON.stringify(loginInfo),
      options).do(resp => {
        if (resp) {
          this.currentUser = <IUser>resp.json().username;
        }
    }).catch(error => {
      return Observable.of(false);
    });
  }
  isAuthenticated() {
    return !!this.currentUser;
  }
}

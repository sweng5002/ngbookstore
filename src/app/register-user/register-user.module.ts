import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RegisterUserRoutingModule } from './register-user-routing.module';

import { RegisterUserComponent } from './register-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RegisterUserRoutingModule
  ],
  declarations: [
    RegisterUserComponent
  ]
})
export class RegisterUserModule {
}

import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
  books = [
    {
    id: 1,
    title: 'Solving Crimes for Dummies',
    author: 'Bruce Wayne',
    price: 19.99,
    img: '../../../assets/images/book.png'
  },
    {
      id: 2,
      title: 'Coding for Dummies',
      author: 'John Doe',
      price: 39.99,
      img: '../../../assets/images/book.png'
    },
    {
      id: 3,
      title: 'Singing for Dummies',
      author: 'Frank Mill',
      price: 37.99,
      img: '../../../assets/images/book.png'
    },
    {
      id: 4,
      title: 'WhoDaMan',
      author: 'I IS DA MAN',
      price: 99.99,
      img: '../../../assets/images/book.png'
    }];
    constructor() {
        this.sliders.push({
            imagePath: 'assets/images/green-lantern.jpg',
            label: 'Epic Reads!',
            text: 'Get your hands on the latest epic reads!!!'
        }, {
            imagePath: 'assets/images/harrypotter.jpg',
            label: 'Second slide label',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        }, {
            imagePath: 'assets/images/to-kill-a-mockingbird.jpg',
            label: 'Class Reads',
            text: 'Brush up on classics, from out classic reads sections!'
        });

        this.alerts.push({
            id: 1,
            type: 'success',
            message: `Click For the Coolest Deals of the Summer!`
        }, {
            id: 2,
            type: 'warning',
            message: `Clearance Section!`
        });
    }

    ngOnInit() {
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServerService } from '../../service.request';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Book } from './book.model';
import { BookService } from '../../book.service';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  animations: [routerTransition()]
})
export class SearchComponent implements OnInit {
  Books: Book[] = [];
   books: Array<Book> = new Array<Book>();

  searchForm: FormGroup;

  searchType: string;

  constructor(private router: Router, private route: ActivatedRoute, private requestService: ServerService,
  private bookService: BookService) {

  }


  ngOnInit() {
    // this.searchType = this.route.snapshot.params['type'];
    this.searchForm = new FormGroup({
      'param': new FormControl(null, [Validators.required]),
    });
  }
  buyBook(selbook: Book) {
    console.log(selbook);
    this.bookService.setBookInfo(selbook);
    this.router.navigate(['/Purchase']);
  }

  getBook() {
this.books = new Array<Book>();
    const dataArrya = this.searchForm.get('param').value;

    console.log(dataArrya);
    this.requestService.getBooks(dataArrya)
      .subscribe(
        response  => {
          console.log(response.json()['items']);

          const body = response.json();
          //  let books: Array<Book> = new Array<Book>();
            const bookResponse = body;
            console.log(bookResponse);
            console.dir(bookResponse);
            for (const book of bookResponse.items) {
              if (book.saleInfo.saleability === 'FOR_SALE') {

                this.books.push({

                  title: book.volumeInfo.title,
                  subTitle: book.volumeInfo.subTitle,
                  categories: book.volumeInfo.categories,
                  authors: book.volumeInfo.authors,
                  rating: book.volumeInfo.rating,
                  pageCount: book.volumeInfo.pageCount,
                  image: book.volumeInfo.imageLinks === undefined ? '' : book.volumeInfo.imageLinks.thumbnail,
                  description: book.volumeInfo.description,
                  isbn: book.volumeInfo.industryIdentifiers,
                  previewLink: book.volumeInfo.previewLink,
                  isforsale: book.saleInfo.saleability,
                  price: book.saleInfo.listPrice.amount,
                  currency: book.saleInfo.listPrice.currencyCode
                });
              }
            }
          console.log('Showing book object after populating');
         console.log(this.books);
        }
        ,
        (error) => console.log(error)

      );
  }
}

import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ServerService } from '../../service.request';
import { BookService } from '../../book.service';
import { SearchComponent } from './search.component';
import { SearchRoutingModule } from './search-routing.module';
import { PageHeaderModule } from '../../shared/modules/page-header/page-header.module';

@NgModule({
  declarations: [
    SearchComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    SearchRoutingModule,
    PageHeaderModule
  ],
  providers: [
    ServerService,
    BookService
  ]
})

export class SearchModule { }

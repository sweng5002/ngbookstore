import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      { path: 'home', loadChildren: './dashboard/dashboard.module#DashboardModule' },
      { path: 'books', loadChildren: './search/search.module#SearchModule' }
      // { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ServerService } from '../../service.request';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {

  constructor (private requestService1: ServerService ){}

  purchaseForm: FormGroup;
  states= [];

  ngOnInit() {
    this.purchaseForm = new FormGroup({
      'line1': new FormControl(null, Validators.required),
      'line2': new FormControl(null),
      'city': new FormControl(null, Validators.required),
      'state': new FormControl(null, Validators.required),
      'zip': new FormControl(null, Validators.required),
      'cardNumber': new FormControl(null, Validators.required),
      'cardcvv': new FormControl(null, Validators.required),
      'cardDate': new FormControl(null, Validators.required)
    });

    this.requestService1.getState().subscribe(
      (data) => this.states = data,
      (error) => console.log(error)
    );
  }

}

// import { Component, OnInit } from '@angular/core';
// import {FormControl, FormGroup, Validators} from '@angular/forms';
// import { ServerService } from '../../service.request';
//
// @Component({
//   selector: 'app-add-address',
//   templateUrl: './add-address.component.html',
//   styleUrls: ['./add-address.component.css']
// })
// export class AddAddressComponent implements OnInit {
//
//   constructor (private requestService1: ServerService ){}
//
//   adrForm: FormGroup;
//   states= [];
//
//   ngOnInit() {
//     this.adrForm = new FormGroup({
//       'line1' : new FormControl(null, Validators.required),
//       'line2' : new FormControl(null),
//       'city' : new FormControl(null, Validators.required),
//       'state' : new FormControl(null, Validators.required),
//       'zip' : new FormControl(null, Validators.required),
//
//
//     });
//     this.requestService1.getState().subscribe(
//       (data) => this.states = data,
//       (error) => console.log(error)
//     );
//   }
//
// }

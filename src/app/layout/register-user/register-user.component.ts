// import { Component, OnInit } from '@angular/core';
// import { AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
// import { ServerService } from '../../service.request';
// import { HttpModule, Response} from '@angular/http';
// // import {request} from "http";
//
// @Component({
//   selector: 'app-register-user',
//   templateUrl: './register-user.component.html',
//   styleUrls: ['./register-user.component.css']
// })
// export class RegisterUserComponent implements OnInit {
//
//
//   regForm: FormGroup;
//
//
//
//   constructor (private requestService: ServerService ){}
//
//
//   ngOnInit(){
//     this.regForm = new FormGroup({
//       'username' : new FormControl(null, [Validators.required]),
//       'email' : new FormControl(null, [Validators.required, Validators.email])
//       // 'password' : new FormControl(null),
//       // 'confirmpass' : new FormControl(null)
//     });
//
//     this.regForm.addControl('password', new FormControl('', [Validators.required]));
//     this.regForm.addControl('confirmpass', new FormControl('', [Validators.compose(
//       [Validators.required, this.validateAreEqual.bind(this)])]));
// // get states info
//
//    // console.log(this.states);
//   //
//   }
//
//
//   private validateAreEqual(fieldControl: FormControl){
//    //  console.log(this.regForm.get('password').value);
//    // console.log(fieldControl.value);
//     return fieldControl.value === this.regForm.get("password").value ? null : { NotEqual: true };
//     // return true;
//   }
//
//   SendUserData() {
//
//     const  dataArrya =
//       { 'username' : this.regForm.get('username').value,
//         'password': this.regForm.get('password').value,
//         'email' : this.regForm.get('email').value
//       };
//
//     console.log(dataArrya);
//     this.requestService.registerUser(dataArrya)
//       .subscribe(
//
//         (response) => console.log(response),
//         (error) => console.log(error)
//       );
//
//   }
//
// }

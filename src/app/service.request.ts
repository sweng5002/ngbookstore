import {Injectable} from '@angular/core';
import {Headers, Http, Response, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class ServerService {
  httpClient: any;

  constructor (private http: Http) {}

  registerUser(dataq: any) {
    const headers = new Headers();
    headers.append('operation', 'register');
    headers.append('service', 'userManagement');

    const options = new RequestOptions({headers: headers});

    return this.http.post('http://18.221.46.7:3010/apigateway', dataq, options);

  }
  getState(){
    const headers = new Headers();
    headers.append('operation', 'getStates');
    headers.append('service', 'userManagement');
    const options = new RequestOptions({headers: headers});
    return this.http.get('http://18.221.46.7:3010/apigateway', options)
      .map(
        (response: Response) => {
          const data = response.json()['states'];
          return data;
        }
      );
  }

  getBooks(searchparam: string) {

    const str = new String(searchparam);
    const query = str.replace(/\s/g, '+');

    // let par = new HttpParams();
    // par.set('q', searchparam);

    //console.log(par);
    return this.http.get('https://www.googleapis.com/books/v1/volumes?q=' + query).map(
      (response: Response) => {
        const data = response;
        return data;
      }
);
}

}
